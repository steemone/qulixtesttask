import UIKit

class InformationViewController: UIViewController {

    private var hourlyModel: HourlyWeatherModel
    private var labelHight = (UIScreen.main.bounds.height / 3)
    private var buttonSize: CGFloat = 44
    
    lazy var tempLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgbColorFor(hex: "4b0082")
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 80, weight: .light)
        label.textAlignment = .right
        return label
    }()
    
    lazy var humidityLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgbColorFor(hex: "9932cc")
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 30, weight: .light)
        label.textAlignment = .right
        return label
    }()
    
    lazy var windSpeedLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgbColorFor(hex: "800080")
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 30, weight: .light)
        label.textAlignment = .right
        return label
    }()
    
    lazy var backButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.rgbColorFor(hex: "4b0082")
        button.setTitle("Back", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(backButton(_:)), for: .touchUpInside)
        button.layer.cornerRadius = 10
        return button
    }()
    
    init(saveModel: HourlyWeatherModel) {
        self.hourlyModel = saveModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        configureTempLabelConstraint()
        configureHumidityLabelConstraint()
        configureWindSpeedLabelConstraint()
        configureBackButtonConstraint()
        configureSetupTempLabel(model: hourlyModel)
    }
    
    @objc func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    private func configureSetupTempLabel(model: HourlyWeatherModel) {
        guard let temp = model.temp,
              let humidity = model.humidity,
              let windSpeed = model.wind_speed else {return}
        self.tempLabel.text = "\(Int(temp))º"
        self.humidityLabel.text = "Влажность \(humidity)%"
        self.windSpeedLabel.text = "Скорость ветра \(windSpeed) м/c"
    }
    private func configureTempLabelConstraint() {
        self.view.addSubview(tempLabel)
        tempLabel.translatesAutoresizingMaskIntoConstraints = false
        tempLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tempLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        tempLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tempLabel.heightAnchor.constraint(equalToConstant: labelHight).isActive = true
    }
    private func configureHumidityLabelConstraint() {
        self.view.addSubview(humidityLabel)
        humidityLabel.translatesAutoresizingMaskIntoConstraints = false
        humidityLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        humidityLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        humidityLabel.topAnchor.constraint(equalTo: self.tempLabel.bottomAnchor).isActive = true
        humidityLabel.heightAnchor.constraint(equalToConstant: labelHight).isActive = true
    }
    private func configureWindSpeedLabelConstraint() {
        self.view.addSubview(windSpeedLabel)
        windSpeedLabel.translatesAutoresizingMaskIntoConstraints = false
        windSpeedLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        windSpeedLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        windSpeedLabel.topAnchor.constraint(equalTo: self.humidityLabel.bottomAnchor).isActive = true
        windSpeedLabel.heightAnchor.constraint(equalToConstant: labelHight).isActive = true
    }
    private func configureBackButtonConstraint() {
        self.view.addSubview(backButton)
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.leadingAnchor.constraint(equalTo: self.tempLabel.leadingAnchor, constant: 10).isActive = true
        backButton.topAnchor.constraint(equalTo: self.tempLabel.topAnchor, constant: 50).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: buttonSize).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: buttonSize).isActive = true
    }
    
}
