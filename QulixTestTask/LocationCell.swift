import UIKit

class LocationCell: UITableViewCell {
    
    private var dateHourly: String = ""
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 40, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.rgbColorFor(hex: "008080")
        configureInfoLabelConstaint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureInfoLabelConstaint() {
        contentView.addSubview(infoLabel)
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        infoLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        infoLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        infoLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
    }
    
    func configure(with object: HourlyWeatherModel) {
        guard let date = object.dt else {return}
        let currentDate = Date(milliseconds: date * 1000)
        dateFormatter(date: currentDate)
        self.infoLabel.text = "\(dateHourly)"
    }
    
    func dateFormatter(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm d.MM.y"
        self.dateHourly = dateFormatter.string(from: date)
    }
    
}
