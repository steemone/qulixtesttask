import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    struct Cells {
        static let locationCell = "LocationCell"
    }
    
    private let locationManager = CLLocationManager()

    private var latitude: Double?
    private var longitude: Double?
    private var hourlyArray: [HourlyWeatherModel] = []
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor.rgbColorFor(hex: "008080")
        table.register(LocationCell.self, forCellReuseIdentifier: Cells.locationCell)
        table.isHidden = false
        table.rowHeight = 50
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureConstraintTableView()
        tableView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        self.latitude = location.latitude
        self.longitude = location.longitude
        downloadWeather()
    }
    
    private func setTableViewDelegates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func configureConstraintTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
    }
    
    private func configureTableView() {
        self.view.addSubview(tableView)
        setTableViewDelegates()
    }
    
    private func downloadWeather() {
        NetworkManager.shared.getWeather(latitude: self.latitude ?? 30, longitude: self.longitude ?? 100, completion:  {  model in
            guard let hourlyArray = model?.hourly else {return}
            self.hourlyArray = hourlyArray
            self.tableView.reloadData()
        })
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.hourlyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cells.locationCell) as! LocationCell
        cell.configure(with: hourlyArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let vc = InformationViewController(saveModel: hourlyArray[indexPath.row])
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 0, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Температура"
        label.font = .systemFont(ofSize: 32)
        label.textAlignment = .center
        label.textColor = .black
        headerView.addSubview(label)
        return headerView
    }
    
}
